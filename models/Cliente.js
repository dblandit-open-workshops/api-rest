const mongoose = require("mongoose");

const Cliente = new mongoose.Schema({
    nombre: { type:String, trim:true },
    apellido: { type:String, trim:true },
    cuit: Number ,
    region: { type:String, trim:true }
});

module.exports = mongoose.model("Cliente", Cliente);