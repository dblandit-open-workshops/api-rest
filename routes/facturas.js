var express = require('express');
var router = express.Router();
const bodyParser = require('body-parser');
const { checkSchema, validationResult } = require('express-validator/check');

const Factura = require("../models/Factura");

var path = require('path');


router.use(bodyParser.urlencoded({ extended: true }));

//middleware toda funcion que tiene acceso al request y response, y puede manipularlos
// ejemplo de middleware que es llamado en todos los request de la api, podriamos usarlo para comprobar si el usuario está logueado o para validar si API KEY
router.use( (req, res, next) => {

    //ejemplo loquea el timestamp del request.
    console.log('Time: ', Date.now());

    next(); //LLama al siguiente middleware, sino el request quedaría en el limbo.
});

//http://localhost:3000/api/facturas/
router.get('/', function (req, res) {

    //{name:{"$split":1}}
    Factura.find(req.query).limit(10).then(function (facturas) {

        res.json(facturas);

    }).catch((err) => {
        console.error(err);
        res.status(500);
        res.send();
    });

});

function findOneFactura(req,res, onSuccess)
{
    Factura.findByNroFactura(req.params.nroFactura).then(function (factura) {

        if(factura == null)
        {
            res.status(404).send();
            return;
        }

        res.json(onSuccess(factura));

    }).catch((err) => {
        console.error(err);
        res.status(500).send();
    });
}

//http://localhost:3000/api/facturas/1001/total
router.get('/:nroFactura/total', function (req, res) {

    findOneFactura(req, res, (factura) => factura.sumar());

});

//http://localhost:3000/api/facturas/1001/total
router.get('/:nroFactura', function (req, res) {

    findOneFactura(req, res, (factura) => factura);

});

router.get('/alta', function (req, res) {

    res.sendFile(path.resolve("./public/facturas/alta.html"));

});

router.post('/', checkSchema({

    nombreCliente: {
        in: ['body'],
        errorMessage: 'El campo nombreCliente esta mal!',
        isString: true
    },
    apellidoCliente: {
        in: ['body'],
        errorMessage: 'El campo apellidoCliente esta mal!',
        isString: true
    },
    CUITCliente: {
        in: ['body'],
        errorMessage: 'El campo CUITCliente esta mal!',
        isInt: true,
        toInt: true
    },
    condPago: {
        in: ['body'],
        errorMessage: 'El campo condPago esta mal!',
        isString:true
    }

}), function (req, res) {

    let validation = validationResult(req).array();

    if(validation.length > 0)
    {
        res.status(400).json(validation);
        return;
    }

    var date = new Date();
    date.setDate(date.getDate() + 10); //sumamos 10 dias a hoy

    var factura = new Factura({
        nroFactura: 10000001,
        condPago: req.body.condPago,
        fechaEmision: new Date(),
        fechaVencimiento: date,
        cliente: {
            nombre: req.body.nombreCliente,
            apellido: req.body.apellidoCliente,
            cuit: req.body.CUITCliente,
        }
    });

    factura.save().then(doc => {

        res.status(201).json(doc); //devolvemos created y lo que creamos.

    }).catch((err) =>{
        console.error(err);
        res.status(500).send();
    });
});

//http://localhost:3000/api/facturas/10000001
router.delete('/:nroFactura', function (req, res) {

    Factura.findOneAndRemove({nroFactura: req.params.nroFactura}).then(function (factura) {

        if(factura == null)
        {
            res.status(404).send();
            return;
        }

        res.json(factura);

    }).catch((err) => {
        console.error(err);
        res.status(500).send();
    });
});

module.exports = router;